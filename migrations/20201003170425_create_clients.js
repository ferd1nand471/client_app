exports.up = async function(knex) {
  await knex.schema.createTable("clients", function(t) {
    t.increments();
    t.string('name');
  });
};

exports.down = async function(knex) {
  await knex.schema.dropTable("clients");
};
