exports.up = async function(knex) {
    const clients = [
        {name: 'john frusciante'},
        {name: 'anthony kiedis'},
        {name: 'chad smith'},
        {name: 'flea'},
        {name: 'kurt cobain'},
        {name: 'krist novocelic'},
        {name: 'dave growl'},
        {name: 'matt bellami'},
        {name: 'chris wolstenholme'},
        {name: 'dominic howard'},
    ]

    await knex('clients').insert(clients);
};

exports.down = async function(knex) {

};
